﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace WindowsFormsApplication1
{
    class Setting
    {
        private static Setting _inst = new Setting();
        private int _v_int;

        protected Setting() 
        {

        }

        public static Setting Instance
        {
            get
            {
                return _inst;
            }
        }
        public int Integer
        {
            get
            {
                return _v_int;
            }
            set
            {
                _v_int = value;
            }

        }
    }
}
